# Script to install mariadb on a linux

hostname=$1
ssh -o StrictHostKeyChecking=no -i ~/.ssh/Amalias_key_AWS.pem ec2-user@$hostname '


# Once logged into your Linux machine 

sudo yum update -y
sudo yum install mariadb-server -y
sudo systemctl start mariadb
sudo systemctl status mariadb
sudo systemctl enable mariadb

sudo yum install wget
https://raw.githubusercontent.com/cwoodruff/ChinookDatabase/master/Scripts/Chinook_MySql.sql

mysql -u root --password="" -e "source Chinook_MySql.sql"
mysql -u root --password="" -e "use Chinook; show tables;"

'